//
//  BluetoothHelpers.swift
//  LEDShirt
//
//  Created by Milo Gilad on 2/20/19.
//  Copyright © 2019 NBPS. All rights reserved.
//

import Foundation
import RxBluetoothKit

protocol BluetoothDevice {
    var name: String { get };
}

class PeripheralBLE: BluetoothDevice {
    var name: String
    init(name: String) {
        self.name = name;
    }
}

func discoverDevices() -> [BluetoothDevice] {
    let manager = CentralManager(queue: .main);
    if manager.state != BluetoothState.poweredOn {
        NSLog("Bluetooth state is not poweredOn!");
        return [];
    }
    
    var result: [PeripheralBLE] = [];
    
    manager.scanForPeripherals(withServices: nil)
        .subscribe({ scannedPeripheral in
            result.append(PeripheralBLE(name: scannedPeripheral.element!.advertisementData.localName!))
        })
    
    return result;
}

func uploadText(to: BluetoothDevice, as: String) -> Bool {
    NSLog("Error: Not implemented");
    return false;
}
